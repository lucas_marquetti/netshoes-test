//
//  DetailGistViewController.swift
//  NetShoesTest
//
//  Created by Lucas Marquetti on 17/06/16.
//  Copyright © 2016 Lucas Marquetti. All rights reserved.
//

import UIKit

class DetailGistViewController: UIViewController {
    
    var gist : Gist!
    @IBOutlet weak var imgOwner: UIImageView!
    @IBOutlet weak var nameOwner: UILabel!
    @IBOutlet weak var typeGist: UILabel!
    @IBOutlet weak var languageGist: UILabel!
    @IBOutlet weak var nameGist: UILabel!
    @IBOutlet weak var sizeGist: UILabel!
    @IBOutlet weak var urlGist: UILabel!
    @IBOutlet weak var contentView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setGist()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setGist(){
        
        self.nameOwner.text = (gist.owner != nil) ?  gist.owner!.name : "--"
        self.typeGist.text = (gist.type != nil) ? gist.type : "--"
        self.languageGist.text = (gist.language != nil) ? gist.language : "--"
        self.nameGist.text = (gist.filename != nil) ? gist.filename : "--"
        self.sizeGist.text = String(gist.size)
        self.urlGist.text = (gist.rawUrl != nil) ? gist.rawUrl : "--"
        
        setupImage()
        setupLayout(contentView)
    }
    
    func setupImage(){
        if let url  = NSURL(string:  (self.gist.owner?.avatarUrl)!),
            data = NSData(contentsOfURL: url)
        {
            imgOwner.image = UIImage(data: data)
        }
    }
    
    func setupLayout(viewBgShadow : UIView){
        viewBgShadow.layer.backgroundColor = UIColor.whiteColor().CGColor
        viewBgShadow.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewBgShadow.layer.borderWidth = 0.2
        viewBgShadow.layer.cornerRadius = 5
        viewBgShadow.layer.masksToBounds = false
    }

    

}
