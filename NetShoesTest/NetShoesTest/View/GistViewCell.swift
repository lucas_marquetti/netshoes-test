//
//  GistTableViewCell.swift
//  NetShoesTest
//
//  Created by Lucas Marquetti on 17/06/16.
//  Copyright © 2016 Lucas Marquetti. All rights reserved.
//

import UIKit

class GistViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgOwner: UIImageView!
    @IBOutlet weak var nameOwner: UILabel!
    @IBOutlet weak var typeGist: UILabel!
    @IBOutlet weak var languageGist: UILabel!
    
    var gist: Gist!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setGistCell(gist : Gist){
        self.gist = gist
        
        self.nameOwner.text = (gist.owner != nil) ?  gist.owner!.name : "--"
        self.typeGist.text = (gist.type != nil) ? gist.type : "--"
        self.languageGist.text = (gist.language != nil) ? gist.language : "--"
        
        setupImage()
        setupLayout(contentView)
    }
    
    func setupImage(){
        if self.gist.owner != nil {
            imgOwner.image = self.gist.owner!.image
        }
    }
    
    
    func setupLayout(viewBgShadow : UIView){
        viewBgShadow.layer.backgroundColor = UIColor.whiteColor().CGColor
        viewBgShadow.layer.borderColor = UIColor.lightGrayColor().CGColor
        viewBgShadow.layer.borderWidth = 0.2
        viewBgShadow.layer.cornerRadius = 5
        viewBgShadow.layer.masksToBounds = false
    }

}
