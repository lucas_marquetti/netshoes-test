//
//  ApiClient.swift
//  NetShoes
//
//  Created by Lucas Marquetti on 16/06/16.
//  Copyright © 2016 Lucas Marquetti. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


let kURL = "https://api.github.com/gists/public?page="

class ApiClient: NSObject {
    
    
    
    func getGist(page: String,  completionClosure: (responseObject: NSMutableArray!, erro :  NSError!) ->()) {
        
        let urlPage = kURL + page
        
        Alamofire.request(.GET, urlPage, parameters: nil)
            .responseJSON { response in
                
                switch response.result {
                  case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        let arrayGist = NSMutableArray()
                        for dict in json.array! {
                       
                            let ownerString = (dict["owner"].dictionaryObject != nil) ? dict["owner"].dictionaryObject :
                                ["login": "--" ,
                                    "avatar_url": "https://avatars2.githubusercontent.com/u/15467072?v=3&s=400" ]
                            var owner : Owner?
                            
                            owner = Owner(dictionary: ownerString!)
                            owner!.setupImage()
                            
                            for attrKey in dict["files"].dictionaryObject! {
                              
                                let gist = Gist(dictionary: attrKey.1 as! [String : AnyObject])
                                    gist.owner = owner
                                
                                arrayGist.addObject(gist)
                            }
                        }
                        
//                        NSUserDefaults.standardUserDefaults().setObject(arrayGist, forKey: "arrayGist")
//                        NSUserDefaults.standardUserDefaults().synchronize()
                        
                        completionClosure(responseObject: arrayGist,  erro: nil)
                    }
                  case .Failure(let error):
                     print(error)
                   // let arrayGist =  NSUserDefaults.standardUserDefaults().objectForKey("arrayGist") as! NSMutableArray
                    completionClosure(responseObject: nil,  erro: error)
                }
         }
    }

}



