//
//  Owner.swift
//  NetShoesTest
//
//  Created by Lucas Marquetti on 16/06/16.
//  Copyright © 2016 Lucas Marquetti. All rights reserved.
//

import UIKit
import JSONHelper

class Owner: NSObject {
    
    static let nameKey = "login"
    static let avatarUrlKey = "avatar_url"
    
    var name : String?
    var avatarUrl : String?
    
    var image : UIImage!
    
    required init(dictionary: [String : AnyObject!]) {
        name <-- dictionary[Owner.nameKey]
        avatarUrl <-- dictionary[Owner.avatarUrlKey]
    }
    
    func setupImage(){
   
        if let url  = NSURL(string: avatarUrl!),
            data = NSData(contentsOfURL: url)
        {
            image = UIImage(data: data)!
        }
    }
}