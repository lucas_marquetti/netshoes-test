//
//  Gist.swift
//  NetShoesTest
//
//  Created by Lucas Marquetti on 16/06/16.
//  Copyright © 2016 Lucas Marquetti. All rights reserved.
//

import UIKit
import JSONHelper

class Gist: NSObject {
    
    static let filenameKey = "filename"
    static let typeKey = "type"
    static let languageKey = "language"
    static let rawUrlKey = "raw_url"
    static let sizeKey = "size"
    
    var owner : Owner?
    
    private(set) var filename : String?
    private(set) var type : String?
    private(set) var language : String?
    private(set) var size : Int = 0
    private(set) var rawUrl : String?
    
    required init(dictionary: [String : AnyObject]) {
        filename <-- dictionary[Gist.filenameKey]
        type <-- dictionary[Gist.typeKey]
        language <-- dictionary[Gist.languageKey]
        size <-- dictionary[Gist.sizeKey]
        rawUrl <-- dictionary[Gist.rawUrlKey]
    }
}
