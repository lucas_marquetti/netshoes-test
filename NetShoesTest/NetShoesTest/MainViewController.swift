//
//  ViewController.swift
//  NetShoesTest
//
//  Created by Lucas Marquetti on 16/06/16.
//  Copyright © 2016 Lucas Marquetti. All rights reserved.
//

import UIKit
import Alamofire

class MainViewController: UIViewController {
    
    private let reuseIdentifier = "GistViewCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    let webServiceWork = ApiClient()
    var page = 0
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    var arrayGist = NSMutableArray()
    var gistSelected : Gist!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getGists()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getGists () {
         indicatorView.hidden = false
        webServiceWork.getGist(String(page)) { (responseObject, erro) in
            if responseObject != nil && responseObject.count > 0 {
                self.arrayGist.addObjectsFromArray(responseObject as [AnyObject])
                self.setLoadCollectionView()
                self.indicatorView.hidden = true
            }
        }
    }
    
    func setLoadCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func getGistForIndexPath(indexPath: NSIndexPath) -> Gist {
        return arrayGist[indexPath.row] as! Gist//.searchResults[indexPath.row]
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueDetail" {
            let viewController:DetailGistViewController = segue.destinationViewController as! DetailGistViewController
            viewController.gist = gistSelected
            
        }
    }
}

extension MainViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayGist.count //searches[section].searchResults.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! GistViewCell
        
        let gist = arrayGist[indexPath.row]
        
        cell.setGistCell(gist as! Gist)
      
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("selecionando")
        gistSelected = getGistForIndexPath(indexPath)
        performSegueWithIdentifier("segueDetail", sender: nil)
    }
}


extension MainViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSize(width: 220, height: 160)
    }
    
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
}

extension MainViewController : UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
      
        
        // Calculate where the collection view should be at the right-hand end item
        let fullyScrolledContentOffset:CGFloat = 150 * CGFloat(arrayGist.count - 1)
         print("Scroll finished", scrollView.contentOffset.y)
        if (scrollView.contentOffset.y >= fullyScrolledContentOffset) {
            
            page += 1
            print("Scroll finished")
            getGists()

        }
        

    }
}
